package main

import (
	"context"
	"log"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"strings"
	"sync"

	"uaa/config"
	"uaa/controllers"
	model "uaa/models"

	"github.com/gorilla/mux"
	pflag "github.com/spf13/flag"
	"github.com/spf13/viper"
)

func withStoreMiddleware(store *model.Store) mux.MiddlewareFunc {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			ctx := context.WithValue(r.Context(), "Store", store)
			next.ServeHTTP(w, r.WithContext(ctx))
		})
	}
}

func gracefulShutdown(srv *http.Server, quitCh <-chan os.Signal, cancel func(), ctx context.Context, wg *sync.WaitGroup) {
	defer cancel()

	<-quitCh

	log.Println("Connection is closing...")

	err := srv.Shutdown(ctx)

	if err != nil {
		log.Fatalf("Cannot close connection: %v", err)
	}

	log.Println("Connection is closed")

	wg.Done()

	os.Exit(0)
}

func main() {
	var configPath *string = pflag.String("config", "./config.yml", "path to config file")

	pflag.Parse()

	viper.SetConfigFile(*configPath)

	var configuration config.Configuration

	if err := viper.ReadInConfig(); err != nil {
		log.Fatalf("Error reading config file, %s", err)
	}
	err := viper.Unmarshal(&configuration)

	if err != nil {
		log.Fatalf("unable to decode into struct, %v", err)
	}

	store := model.NewStore()

	r := mux.NewRouter()

	r.Use(withStoreMiddleware(store))

	r.HandleFunc("/items", controllers.Create).Methods("POST")
	r.HandleFunc("/items/{id}", controllers.Get).Methods("GET")
	r.HandleFunc("/items/{id}", controllers.Remove).Methods("DELETE")
	r.HandleFunc("/items/{id}", controllers.Edit).Methods("PUT")
	r.HandleFunc("/items/", controllers.List).Methods("GET")

	srv := &http.Server{
		Addr:    strings.Join([]string{configuration.HttpListen.Ip, strconv.Itoa(configuration.HttpListen.Port)}, ":"),
		Handler: r,
	}

	go func() {
		err = srv.ListenAndServe()

		if err != nil {
			log.Fatalf("server can not start, %v", err)
		}
	}()

	log.Printf("Server start lisening on ip %s and port %d", configuration.HttpListen.Ip, configuration.HttpListen.Port)

	quitCh := make(chan os.Signal, 1)

	signal.Notify(quitCh, os.Interrupt)

	wg := &sync.WaitGroup{}
	wg.Add(1)

	ctx, cancel := context.WithCancel(context.Background())

	go gracefulShutdown(srv, quitCh, cancel, ctx, wg)

	wg.Wait()
}
