package controllers

import (
	"encoding/json"
	"log"
	"net/http"
	"strconv"
	model "uaa/models"

	"github.com/gorilla/mux"
)

func Create(w http.ResponseWriter, r *http.Request) {
	var i interface{}

	err := json.NewDecoder(r.Body).Decode(&i)

	if err != nil {
		log.Printf("unable to create new item, %v", err)

		return
	}

	storeCtx := r.Context().Value("Store")

	if storeCtx == nil {
		w.WriteHeader(http.StatusInternalServerError)

		return
	}

	store := storeCtx.(*model.Store)

	store.Add(i)
}

func Remove(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)

	id, err := strconv.Atoi(params["id"])

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
	}

	storeCtx := r.Context().Value("Store")

	if storeCtx == nil {
		w.WriteHeader(http.StatusInternalServerError)

		return
	}

	store := storeCtx.(*model.Store)

	item := store.Get(id)

	if item == nil {
		w.WriteHeader(http.StatusNotFound)

		return
	}

	if item := store.Remove(id); item == nil {
		w.WriteHeader(http.StatusInternalServerError)

		return
	}

	w.WriteHeader(http.StatusOK)

	// todo retur item
}

func Get(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)

	id, err := strconv.Atoi(params["id"])

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
	}

	storeCtx := r.Context().Value("Store")

	if storeCtx == nil {
		w.WriteHeader(http.StatusInternalServerError)

		return
	}

	store := storeCtx.(*model.Store)

	item := store.Get(id)

	if item == nil {
		w.WriteHeader(http.StatusNotFound)

		return
	}

	json.NewEncoder(w).Encode(item)
}

func List(w http.ResponseWriter, r *http.Request) {
	storeCtx := r.Context().Value("Store")

	if storeCtx == nil {
		w.WriteHeader(http.StatusInternalServerError)

		return
	}

	store := storeCtx.(*model.Store)

	ids := store.Ids()

	result := make([]*model.Item, 0)

	for _, id := range ids {
		item := store.Get(id)

		result = append(result, item)
	}

	json.NewEncoder(w).Encode(result)
}

func Edit(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)

	id, err := strconv.Atoi(params["id"])

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
	}

	var i interface{}

	err = json.NewDecoder(r.Body).Decode(&i)

	if err != nil {
		log.Printf("can not decode, %v", err)

		return
	}

	storeCtx := r.Context().Value("Store")

	if storeCtx == nil {
		w.WriteHeader(http.StatusInternalServerError)

		return
	}

	store := storeCtx.(*model.Store)

	item := store.Get(id)

	if item == nil {
		w.WriteHeader(http.StatusNotFound)

		return
	}

	if item := store.Set(id, i); item == nil {
		w.WriteHeader(http.StatusInternalServerError)

		return
	}

	w.WriteHeader(http.StatusOK)

	// todo retur item
}
