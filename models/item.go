package model

type Item struct {
	id    int         `json:"Id"`
	Value interface{} `json:"Value"`
}
