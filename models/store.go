package model

type Store struct {
	byKey  map[int]*Item
	lastId int
	ids    []int //bad idea, refactor it similar to this one: https://gist.github.com/jaybekster/21c516fdb5251c65c17452dc9d8cc3b9 to make iteration and access faster
}

func NewStore() *Store {
	store := new(Store)
	store.lastId = 1
	store.byKey = make(map[int]*Item)
	store.ids = make([]int, 0, 10)

	return store
}

func (store *Store) Add(value interface{}) {
	item := new(Item)
	item.id = store.lastId
	item.Value = value
	store.byKey[store.lastId] = item
	store.ids = append(store.ids, store.lastId)
	store.lastId++
}

func (store *Store) Remove(id int) *Item {
	if item, ok := store.byKey[id]; ok {
		delete(store.byKey, id)

		for idx := range store.ids {
			if store.ids[idx] == id {
				store.ids = append(store.ids[0:idx], store.ids[idx+1:len(store.ids)]...)
			}
		}

		return item
	}

	return nil
}

func (store *Store) Get(id int) *Item {
	if item, ok := store.byKey[id]; ok {
		return item
	}

	return nil
}

func (store *Store) Set(id int, payload interface{}) *Item {
	if item, ok := store.byKey[id]; ok {
		store.byKey[id].Value = payload

		return item
	}

	return nil
}

func (store *Store) Ids() []int {
	// return not a pointer to disallow changing this array outside
	return store.ids
}
