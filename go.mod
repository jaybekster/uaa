module uaa

go 1.15

require (
	github.com/gorilla/mux v1.8.0
	github.com/spf13/flag v0.0.0-20140426202619-916eaf9e2dc5
	github.com/spf13/viper v1.7.1
)
